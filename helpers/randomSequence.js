const adjectives = require('../assets/adjectives-en.json')
const animals = require('../assets/animals-en.json')
const superb = require('../assets/superb-en.json')

function pickOne (list) {
  const index = Math.floor(Math.random() * list.length)
  return list[index]
}

function randomSequence () {
  const prefix = pickOne(superb)
  const adjective = pickOne(adjectives)
  const animal = pickOne(animals)
  return `${prefix}-${adjective}-${animal}`
}

module.exports = randomSequence
