const {join} = require('path')
const {promisify} = require('util')
const mkdirp = require('mkdirp')

module.exports = async (fastify, options) => {
  const server = require('fastify')({logger: true})
  const root = join(options.webroot, options.prefix)
  await promisify(mkdirp)(root)
  server.register(require('fastify-static'), {
    root,
    prefix: options.prefix
  })
  fastify.addHook('onClose', (fastify, done) => {
    server.close(done)
  })
  await server.listen(options.port, '::')
}

module.exports[Symbol.for('skip-override')] = true
