const jwtPermissions = require('express-jwt-permissions')
const {modifyUser} = require('../../../hooks/modifyUser')
const isValidDomain = require('is-valid-domain')
const isDomainName = require('is-domain-name')
const {Gone} = require('http-errors')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check('deploy')
  )

  fastify.route({
    method: 'GET',
    url: '/domains/available',
    schema: {
      querystring: {
        domain: {type: 'string'}
      }
    },
    beforeHandler: [
      modifyUser()
    ],
    handler: async (request, reply) => {
      const {db} = fastify.mongo
      if (request.query.domain) {
        const {domain} = request.query
        if (isDomainName(domain) && isValidDomain(domain)) {
          const conflict = await db.collection('hosts').findOne({domain})
          if (!conflict) {
            reply.send()
            return
          }
        }
      }
      throw new Gone('Domain unavailable')
    }
  })
}
