const jwtPermissions = require('express-jwt-permissions')
const {modifyUser} = require('../../../../hooks/modifyUser')
const {checkDomainAccess} = require('../../../../hooks/checkDomainAccess')

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check([['deploy'], ['global_read']])
  )

  fastify.route({
    method: 'GET',
    url: '/sites/:domain/configuration',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            modified: {type: 'string'},
            domain: {type: 'string'},
            configuration: {
              type: 'object',
              additionalProperties: true
            }
          }
        }
      }
    },
    beforeHandler: [
      modifyUser(),
      checkDomainAccess()
    ],
    handler: async (request, reply) => {
      const {domain} = request.params
      const {db} = fastify.mongo

      return db.collection('configurations')
        .findOne({domain})
    }
  })
}
