const jwtPermissions = require('express-jwt-permissions')

const NOT_MODIFIED = 304

module.exports = async (fastify, options) => {
  fastify.use(
    jwtPermissions({permissionsProperty: 'scope'})
      .check('global_read')
  )

  fastify.route({
    method: 'GET',
    url: '/configurations',
    schema: {
      response: {
        200: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              modified: {type: 'string'},
              domain: {type: 'string'},
              configuration: {
                type: 'object',
                additionalProperties: true
              }
            }
          }
        }
      }
    },
    handler: async (request, reply) => {
      const {db} = fastify.mongo
      const since = Date.parse(request.headers['if-modified-since'])
      const filter = isNaN(since) ? {} : {modified: {$gte: new Date(since)}}
      const configurations = await db.collection('configurations')
        .find(filter).toArray()

      const lastModified = configurations
        .reduce((last, {modified}) => modified > last ? modified : last, '')
      if (lastModified !== '') {
        reply.header('last-modified', lastModified)
      }

      if (configurations.length === 0) {
        reply.code(NOT_MODIFIED).send()
      } else {
        return configurations
      }
    }
  })
}
